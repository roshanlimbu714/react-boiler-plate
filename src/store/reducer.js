import { combineReducers } from 'redux'
import modalReducer from "../common/Modal/services/modalReducer";
import toasterReducer from '../common/Toaster/services/ToasterReducer'
import loadingReducer from "../common/Loading/services/loadingReducer";
import authReducer from "../entities/auth/services/authReducer";

export default combineReducers({
    modalReducer,
    toasterReducer,
    loadingReducer,
    authReducer
})
