import React from 'react';
import {Switch, Route} from "react-router";
import routes from '../routes';
import PrivateRoute from "./PrivateRoutes";

const AppLayout = props => {
    return (
        <Switch>
            {

                routes.map((route, key) => (
                    !route.isAuth
                        ? <Route
                            path={`${route.path}`}
                            name={route.name}
                            component={route.component}
                            exact={route.exact}
                            key={key}
                        />
                        : <PrivateRoute
                            path={`${route.path}`}
                            name={route.name}
                            component={route.component}
                            exact={route.exact}
                            key={key}
                        />
                ))
            }
        </Switch>
    )
}

export default AppLayout;
