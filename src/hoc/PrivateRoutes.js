import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import * as actions from '../store/actions'
import {Route, Redirect} from 'react-router-dom'

const PrivateRoute = ({component: Component, setToasterState, ...rest}) => {
    const dispatch = useDispatch()

    const {isLoggedIn} = useSelector(state => state.authReducer)
    const openToaster = () => {
        dispatch(actions.setToasterState(
            true,
            "error",
            "Authentication Error",
            "You have to authenticate before proceedings "
        ))
    }

    useEffect(() => {
        !isLoggedIn && openToaster()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoggedIn])

    return (
        <Route {...rest} render={
            props => !isLoggedIn ? (<Redirect to="/"/>) :
                (<Component {...props} />)
        }
        />
    );
};

export default PrivateRoute
