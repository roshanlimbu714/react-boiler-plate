import Auth from "../entities/auth";
import Landing from "../entities/landing";
import Admin from "../entities/admin";

export default [
    {
        path:'/',
        component:Landing,
        name:"landing",
        exact:true,
        isAuth:false
    },
    {
        path:'/auth',
        component:Auth,
        name:"auth",
        exact:true,
        isAuth:false
    },
    {
        path:'/dashboard',
        component:Admin,
        name:"admin.dashboard",
        exact:true,
        isAuth:true
    },
];
