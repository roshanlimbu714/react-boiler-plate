import React from 'react';
import './App.scss';
import AppLayout from "./hoc/AppLayout";
function App() {
  return (
    <AppLayout/>
  );
}

export default App;
