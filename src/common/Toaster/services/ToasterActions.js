import * as actionTypes from "./ToasterTypes";

export const setToasterState = (state, title,name,message) => {
  return {
    type: actionTypes.SET_TOASTER,
    appear: state,
    title: title,
    name: name,
    message: message,
  };
};
