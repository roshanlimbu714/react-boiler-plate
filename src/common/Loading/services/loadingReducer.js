import * as actionTypes from './loadingType'

const initialState = {
    isLoading: true,
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.SET_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case actionTypes.CLEAR_LOADING:
            return initialState
        default:
            return state
    }
}
