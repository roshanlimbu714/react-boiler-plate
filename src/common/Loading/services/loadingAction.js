import * as actionType from './loadingType'

export const setLoading = ( mode ) => {
    return {
        type: actionType.SET_LOADING,
        mode: mode
    }
}

export const clearLoading = () => {
    return {
        type: actionType.CLEAR_LOADING
    }
}
