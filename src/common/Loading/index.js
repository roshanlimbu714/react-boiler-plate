import React from 'react';
import './loading.scss'
import {useSelector} from "react-redux";

const Loading = props => {
    const {isLoading} = useSelector(state => state.loadingReducer)
    return (
        <>
            {
                isLoading && (
                    <div className="loader-container">
                        <div className="lds-ring">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                )
            }
        </>
    );
};

export default Loading;
