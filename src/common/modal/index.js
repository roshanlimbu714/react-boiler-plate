import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

import * as actions from './services/modalAction'

import './modal.scss'


const Modal = props => {
    const { isOpenModal, closeModal, children } = props;
    return (
        <>
            {
                isOpenModal && (
                    <div className="modal">
                        <div className="modal-backdrop" onClick={() => {
                            closeModal();
                        }}/>
                        <div className="modal-card">
                            <div className="modal-area">
                                { children }
                            </div>
                        </div>
                    </div >
                )

            }
        </>
    )
};

//props type checking
Modal.propTypes = {
    isOpenModal: PropTypes.bool.isRequired,
    closeModal:PropTypes.func.isRequired,
    children: PropTypes.any.isRequired,
};


const mapStateToProps = state => {
    return {
        isOpenModal: state.modalReducer.isOpenModal
    };
};

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(actions.closeModal())
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Modal);
