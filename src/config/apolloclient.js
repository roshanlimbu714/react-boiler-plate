import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";

//cache variable that hold cache values.
const cache = new InMemoryCache();

const link = new HttpLink({
    //graphql api uri
    uri: process.env.REACT_APP_DB_URL
});

const apolloClient = new ApolloClient({
    cache,
    link
});

export default apolloClient;
