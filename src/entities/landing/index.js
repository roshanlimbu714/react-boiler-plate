import React from "react";
import './landing.scss'
import {useHistory} from "react-router-dom";
import Toaster from "../../common/Toaster/Toaster";
import Loading from "../../common/Loading";
import {useDispatch, useSelector} from "react-redux";
import Modal from "../../common/Modal";
import * as actions from '../../store/actions'

const Landing = props => {
        const history   = useHistory()
        const {mode}    = useSelector(state => state.modalReducer)
        const dispatch  = useDispatch()

        return (
            <main>
                <Toaster/>
                <Loading/>
                <nav>This is the landing nav welcome</nav>
                <button onClick={() => history.push('/dashboard')}>go to admin </button>
                <button onClick={() => dispatch(actions.openModal('open'))}> Open modal</button>
                {
                    mode === "open" && <Modal>this is open modal lol </Modal>
                }
            </main>
        )
    }
;

export default Landing;
