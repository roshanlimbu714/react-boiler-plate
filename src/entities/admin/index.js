import React from "react";
import AdminNav from "./containers/AdminNav";
import './index.scss';
import AdminSidebar from "./containers/AdminSidebar";

const Admin = props => {
    return (
        <main>
            <AdminNav/>
        <section>
            <AdminSidebar/>
            <div className="content">
                <div className="form-group">
                    <label htmlFor="">random label</label>
                    <div className="">
                        <input type="text"/>
                    </div>
                    <span className="error-text"></span>
                </div>
            </div>
        </section></main>
    )
}

export default Admin;
