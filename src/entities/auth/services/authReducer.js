import * as actionTypes from './authType'

const initialState = {
    isLoggedIn: false,
    user:{},
}

export default (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.LOGIN_USER:
            return {
                ...state,
                isLoggedIn: true,
                user: actions.user && actions.user,
            };
        case actionTypes.LOGOUT_USER:
            return initialState
        default:
            return state
    }
}
