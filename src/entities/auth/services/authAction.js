import * as actionType from './authType'

export const authSuccess = ( user )  => {
    return {
        type: actionType.AUTH_SUCCESS,
        user: user
    }
}

export const authFail = () => {
    return {
        type: actionType.AUTH_FAIL
    }
}
